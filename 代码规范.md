# 代码规范

## 一、空格

1. 方法换行之后，没必要空格，在代码块前后需要空格

```swift
open class func availableLanguages(_ excludeBase: Bool = false) -> [String] {
    var availableLanguages = Bundle.main.localizations
    
    if let indexOfBase = availableLanguages.index(of: "Base") , excludeBase == true {
        availableLanguages.remove(at: indexOfBase)
    }
        
    return availableLanguages
}

```

2. `switch-case` 之间不需要外的空格

3. 所有的条件分支语句仅仅是为了处理异常情况的，删除不应该影响到主流程(除了guard 返回)， 其他地方尽可能少出现 return